<?php

/**
 * @file
 * Post update functions for No Referrer module.
 */

/**
 * Rebuild container due to refactoring.
 */
function noreferrer_post_update_refactoring(): void {
}
