<?php

namespace Drupal\noreferrer\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\noreferrer\Allowlist\Publisher;
use Drupal\noreferrer\Allowlist\Subscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements a noreferrer Config form.
 */
class NoReferrerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  final public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
    protected Publisher $publisher,
    protected Subscriber $subscriber,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('file_url_generator'),
      $container->get(Publisher::class),
      $container->get(Subscriber::class),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'noreferrer_settings_form';
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames(): array {
    return ['noreferrer.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The settings form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['noreferrer'] = [
      '#config_target' => 'noreferrer.settings:noreferrer',
      '#type'          => 'checkbox',
      '#title'         => $this->t('Add <code>rel="noreferrer"</code> to external links'),
      '#description'   => $this->t('If checked, <code>rel="noreferrer"</code> will be added to non-allowed external links.'),
    ];
    $form['noopener'] = [
      '#config_target' => 'noreferrer.settings:noopener',
      '#type'          => 'checkbox',
      '#title'         => $this->t('Add <code>rel="noopener"</code> if link has a target'),
      '#description'   => $this->t('If checked, <code>rel="noopener"</code> will be added to links with a target attribute.'),
    ];
    $form['referrerpolicy'] = [
      '#config_target' => 'noreferrer.settings:referrerpolicy',
      '#type'          => 'checkbox',
      '#title'         => $this->t('Add <code>referrerpolicy="no-referrer"</code> to external resources in user-generated content'),
      '#description'   => $this->t('If checked, <code>referrerpolicy="no-referrer"</code> will be added to iframes, images and other resources in user-generated content with non-allowed external URLs.'),
    ];
    $form['allowed_domains'] = [
      '#config_target' => new ConfigTarget(
        'noreferrer.settings',
        'allowed_domains',
        fn($value) => isset($value) ? implode(' ', $value) : '',
        [static::class, 'stringToList'],
      ),
      '#type'          => 'textfield',
      '#title'         => $this->t('Allowed domains'),
      '#description'   => $this->t('Enter a space-separated list of domains to which referrer URLs will be sent (e.g. <em>example.com example.org</em>). All other domains will have <code>referrerpolicy="no-referrer"</code> or <code>rel="noreferrer"</code> added.'),
      '#maxlength'     => NULL,
    ];
    $form['publish'] = [
      '#config_target' => 'noreferrer.settings:publish',
      '#type'          => 'checkbox',
      '#title'         => $this->t('Publish list of allowed domains'),
      '#description'   => $this->t('If checked, the list of allowed domains will be published at <a href="@url">@url</a> when saving this form.', [
        '@url' => $this->fileUrlGenerator->generateAbsoluteString($this->publisher->getPublishUri()),
      ]),
    ];
    $form['subscribe_url'] = [
      '#config_target' => new ConfigTarget(
        'noreferrer.settings',
        'subscribe_url',
        NULL,
        [static::class, 'stringToNullable'],
      ),
      '#type'          => 'url',
      '#title'         => $this->t('Subscribe to external list of allowed domains'),
      '#description'   => $this->t('If configured, the list of allowed domains will be retrieved from the given URL during each cron run.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    if ($form_state->getValue('publish')) {
      $this->publisher->publish();
    }
    $url = $form_state->getValue('subscribe_url');
    if ($url && \is_string($url)) {
      $this->subscriber->subscribe($url);
    }
  }

  /**
   * Converts space-delimited string to array.
   *
   * @return string[]
   *   Configuration as an array of strings.
   */
  public static function stringToList(string $string): array {
    return array_values(array_filter(explode(' ', $string)));
  }

  /**
   * Converts empty string to null.
   */
  public static function stringToNullable(string $string): ?string {
    return $string === '' ? NULL : $string;
  }

}
