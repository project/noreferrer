<?php

namespace Drupal\noreferrer\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\Attribute\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\Plugin\FilterInterface;
use Drupal\noreferrer\Allowlist\Validator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to apply the noreferrer attribute.
 */
#[Filter(
  id: 'noreferrer',
  title: new TranslatableMarkup('Add referrerpolicy=&quot;no-referrer&quot;, rel=&quot;noopener&quot; and/or rel=&quot;noreferrer&quot;'),
  description: new TranslatableMarkup('Note, this filter includes the <em>Correct faulty and chopped off HTML</em> filter; there is no need to enable both filters.'),
  type: FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
  weight: 10,
)]
class NoReferrerFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs No Referrer filter.
   *
   * @param mixed[] $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\noreferrer\Allowlist\Validator $validator
   *   The allowlist validator.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory, protected Validator $validator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   * @param mixed[] $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get(Validator::class),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $html_dom = Html::load($text);
    $config = $this->configFactory->get('noreferrer.settings');
    $noopener = $config->get('noopener');
    $noreferrer = $config->get('noreferrer');
    if ($noopener || $noreferrer) {
      foreach (['a' => 'href', 'area' => 'href', 'form' => 'action'] as $tag => $attribute) {
        foreach ($html_dom->getElementsByTagName($tag) as $element) {
          $types = [];
          if ($noopener && $element->getAttribute('target') !== '') {
            $types[] = 'noopener';
          }
          if ($noreferrer && ($href = $element->getAttribute($attribute)) && UrlHelper::isExternal($href) && !$this->validator->isAllowed($href)) {
            $types[] = 'noreferrer';
          }
          if ($types) {
            // Merge existing rel values.
            if ($rel = $element->getAttribute('rel')) {
              // Clear empty strings and ensure all values are unique.
              if ($values = preg_split('/\s+/', $rel, -1, PREG_SPLIT_NO_EMPTY)) {
                $types = array_unique(array_merge($types, $values));
              }
            }
            $element->setAttribute('rel', implode(' ', $types));
          }
        }
      }
    }
    if ($config->get('referrerpolicy')) {
      foreach ([
        'img' => 'src',
        'iframe' => 'src',
        'link' => 'href',
        'script' => 'src',
      ] as $tag => $attribute) {
        foreach ($html_dom->getElementsByTagName($tag) as $element) {
          if (($href = $element->getAttribute($attribute)) && UrlHelper::isExternal($href) && !$this->validator->isAllowed($href)) {
            $element->setAttribute('referrerpolicy', 'no-referrer');
          }
        }
      }
    }
    $result = new FilterProcessResult(Html::serialize($html_dom));
    $result->addCacheTags($config->getCacheTags());
    return $result;
  }

}
