<?php

namespace Drupal\noreferrer\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\noreferrer\Allowlist\Subscriber;

/**
 * Implements hook_cron().
 */
#[Hook('cron')]
class Cron {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected Subscriber $subscriber,
  ) {
  }

  /**
   * Implements hook_cron().
   */
  public function __invoke(): void {
    $url = $this->configFactory->get('noreferrer.settings')->get('subscribe_url');
    if ($url && \is_string($url)) {
      $this->subscriber->subscribe($url);
    }
  }

}
