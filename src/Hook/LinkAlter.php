<?php

namespace Drupal\noreferrer\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\noreferrer\Allowlist\Validator;

/**
 * Implements hook_link_alter().
 */
#[Hook('link_alter')]
class LinkAlter {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected Validator $validator,
  ) {
  }

  /**
   * Implements hook_link_alter().
   *
   * @param array{options: array{attributes?: mixed[]}, url: \Drupal\Core\Url} $variables
   *   Array of link variables.
   */
  public function __invoke(array &$variables): void {
    $config = $this->configFactory->get('noreferrer.settings');
    if (isset($variables['options']['attributes']['rel']) && \is_string($variables['options']['attributes']['rel'])) {
      $variables['options']['attributes']['rel'] = preg_split('/\s+/', $variables['options']['attributes']['rel'], -1, PREG_SPLIT_NO_EMPTY);
    }
    if ($config->get('noopener') && isset($variables['options']['attributes']['target']) && $variables['options']['attributes']['target'] !== '') {
      $variables['options']['attributes']['rel'][] = 'noopener';
    }
    if ($config->get('noreferrer') && $variables['url']->isExternal() && !$this->validator->isAllowed($variables['url']->toString())) {
      $variables['options']['attributes']['rel'][] = 'noreferrer';
    }
  }

}
