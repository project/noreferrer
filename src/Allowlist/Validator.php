<?php

namespace Drupal\noreferrer\Allowlist;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Validates whether or not a URL is allowed.
 */
class Validator {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * Helper function to determine if a host is in the domain allowlist.
   */
  public function isAllowed(string $url): bool {
    $allowed_domains = $this->configFactory->get('noreferrer.settings')->get('allowed_domains');
    if (!$allowed_domains || !\is_array($allowed_domains)) {
      return FALSE;
    }
    $host = parse_url($url, PHP_URL_HOST);
    if (!$host) {
      return FALSE;
    }
    foreach ($allowed_domains as $domain) {
      if (!strcasecmp($domain, $host) || strripos($host, '.' . $domain) === \strlen($host) - \strlen($domain) - 1) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
