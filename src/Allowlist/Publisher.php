<?php

namespace Drupal\noreferrer\Allowlist;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\PrivateKey;

/**
 * Publishes domain allowlist.
 */
class Publisher {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected FileSystemInterface $fileSystem,
    protected PrivateKey $privateKey,
  ) {
  }

  /**
   * Publishes domain allowlist.
   */
  public function publish(): void {
    $allowed_domains = $this->configFactory->get('noreferrer.settings')->get('allowed_domains');
    if ($json = json_encode(\is_array($allowed_domains) ? $allowed_domains : [])) {
      $this->fileSystem->saveData($json, $this->getPublishUri(), FileExists::Replace);
    }
  }

  /**
   * Returns domain allowlist URI.
   */
  public function getPublishUri(): string {
    // For security through obscurity purposes, the allowlist URL is secret.
    return 'public://noreferrer-allowlist-' . Crypt::hmacBase64('noreferrer-allowlist', $this->privateKey->get()) . '.json';
  }

}
