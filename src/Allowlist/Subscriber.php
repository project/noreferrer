<?php

namespace Drupal\noreferrer\Allowlist;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Retrieves allowlist from external URL.
 */
class Subscriber {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected ClientInterface $httpClient,
    protected LoggerChannelFactoryInterface $loggerFactory,
  ) {
  }

  /**
   * Retrieves allowlist from external URL.
   */
  public function subscribe(string $url): void {
    try {
      $response = $this->httpClient->request('GET', $url);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('noreferrer')->error('Error received at %url while retrieving domain allowlist: %message.', [
        '%url' => $url,
        '%message' => $e->getMessage(),
      ]);
      return;
    }
    $allowed_domains = json_decode((string) $response->getBody());
    if (\is_array($allowed_domains)) {
      $this->configFactory->getEditable('noreferrer.settings')
        ->set('allowed_domains', array_filter($allowed_domains, 'is_string'))
        ->save();
    }
    else {
      $this->loggerFactory->get('noreferrer')->error('Unable to extract valid data from %url while retrieving domain allowlist.', ['%url' => $url]);
    }
  }

}
